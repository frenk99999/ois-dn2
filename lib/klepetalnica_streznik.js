var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var up = [];
var trenutniKanal = {};
var gesla = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj' , '');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZasebnoPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('osebe', function() {
      var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
      if (uporabnikiNaKanalu.length > 0) {
        var uporabnikiNaKanaluPovzetek = '';
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += '\n';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
        socket.emit('osebe', {besedilo: uporabnikiNaKanaluPovzetek});
      }
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  up.push(socket.id);
  return stGosta + 1;
}

function obstajaKanal(argument){
  var fu = io.sockets.manager.rooms;
  var je = false;
  for(var i in fu) {
    i = i.substring(1, i.length);
    if (i==argument) {
      je==true;
      break;
    }
  }
  return je;
}

function obdelajGesla(argument,argument2,argument3) {
  if(!obstajaKanal(argument)){
    delete gesla[argument];
  }
  if(!obstajaKanal(argument2)){
    gesla[argument2]=argument3;
  }
}

function pridruzitevKanalu(socket, kanal, geslo) {
  obdelajGesla(trenutniKanal[socket.id],kanal,geslo);
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  
  socket.emit('pridruzitevOdgovor', {kanal: kanal, uspesno: true });
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        up.push(socket.id);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        delete up[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + '\n: ' + sporocilo.besedilo
    });
  });
}

function obdelajZasebnoPosredovanjeSporocila(socket) {
  socket.on('zasebno', function (sporocilo) {
    if(vzdevkiGledeNaSocket[socket.id] == sporocilo.oseba){
      socket.emit('zasebnoUspOdg',{besedilo: 'Sporočilo '+sporocilo.besedilo+' uporabniku z vzdevkom '+sporocilo.oseba+' ni bilo mogoče posredovati.'});
    }
    else{
    var k=uporabljeniVzdevki.indexOf(sporocilo.oseba);
      if(k>-1){
         io.sockets.socket(up[k]).emit('sporocilo', {
          besedilo: vzdevkiGledeNaSocket[socket.id] + ' \n(zasebno): ' + sporocilo.besedilo
        });
        socket.emit('sporocilo',{besedilo: '(zasebno za '+sporocilo.oseba+')\n: '+sporocilo.besedilo});
      }
      else{
         socket.emit('zasebnoUspOdg',{besedilo: 'Sporočilo \n'+sporocilo.besedilo+'\n uporabniku z vzdevkom '+sporocilo.oseba+' ni bilo mogoče posredovati.'});
      }
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if(gesla[kanal.novKanal]==kanal.novgeslo || typeof gesla[kanal.novKanal] === 'undefined' && !obstajaKanal(kanal.novKanal)){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal, kanal.novgeslo);
    }
    else {
      var k = 0;
      if(kanal.novgeslo!='' && gesla[kanal.novKanal]==''){
        k=0;
      }
      else{
        k=1;
      }
      socket.emit('pridruzitevOdgovor', {kanal: trenutniKanal[socket.id], uspesno: false, razlog: ''+k, neKanal: kanal.novKanal });
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete up[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}