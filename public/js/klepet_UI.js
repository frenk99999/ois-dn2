function zasciti(nevarno){
  var varno = '';
  var spreminjaj = true;
  for(var i=0;i<nevarno.length;i++){
    if(nevarno.charAt(i) == '<' && spreminjaj){
      varno = varno + '&#60;';
    }
    else if (nevarno.charAt(i) == '>' && spreminjaj){
      varno = varno + '&#62;';
    }
    else if (nevarno.charAt(i) == '\n'){
      spreminjaj = !spreminjaj;
    }
    else{
      varno = varno + nevarno.charAt(i);
    }
  }
  return varno;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlSporocilo(sporocilo){
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + zasciti(sporocilo) + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = klepetApp.filtriraj(sporocilo);
    sporocilo = klepetApp.predelaj(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text().split(' @ ')[1], sporocilo);
    $('#sporocila').append(divElementHtmlSporocilo(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    var kanalOseba = $('#kanal').text().split('@');
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek+' @'+kanalOseba[1]);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    var kanalOseba = $('#kanal').text().split('@');
    $('#kanal').text(kanalOseba[0]+'@ '+rezultat.kanal);
    if(rezultat.uspesno){
      $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    }
    else{
      if(rezultat.razlog=='0'){
        $('#sporocila').append(divElementHtmlTekst('Izbrani kanal '+ rezultat.neKanal  +' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev '+ rezultat.neKanal +' ali zahtevajte kreiranje kanala z drugim imenom.'));
      }
      else{
        $('#sporocila').append(divElementHtmlTekst('Pridru&#382;itev v kanal '+ rezultat.neKanal  +' ni bilo uspe&#353;no, ker je geslo napa&#269;no!'));
      }
    }
  });

  socket.on('sporocilo', function (sporocilo) {
    sporocilo.besedilo = sporocilo.besedilo.split('\n');
    sporocilo.besedilo[0] = zasciti(sporocilo.besedilo[0]);
    sporocilo.besedilo = sporocilo.besedilo.join('');
    $('#sporocila').append(divElementHtmlSporocilo(sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text() );
      $('#poslji-sporocilo').focus();
    });
  });

 socket.on('osebe', function(osebe) {
    $('#seznam-oseb').empty();
    var os = osebe.besedilo.split('\n');
    for(var i=0;i<os.length;i++){
      $('#seznam-oseb').append(divElementEnostavniTekst(os[i]));
    }
    
  });
  
  socket.on('zasebnoUspOdg',function(rezultat) {
      $('#sporocila').append(divElementHtmlTekst(rezultat.besedilo));
  });
  
  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  setInterval(function() {
    socket.emit('osebe');
  }, 1000);
  
  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});