var Klepet = function(socket) {
  this.socket = socket;
};

var zamenjava = [['(y)','like.png'],
                 [':(','sad.png'],
                 [':)','smiley.png'],
                 [':*','kiss.png'],
                 [';)','wink.png'],
                 ['<','&#60;'],
                 ['>','&#62;']];
                 
Klepet.prototype.predelaj = function(sporocilo){
  var drugoS='';
  for(var i=0;i<sporocilo.length;i++){
    var zam=-1;
    for(var j=0;j<zamenjava.length;j++){
      var k=0;
      while(zamenjava[j][0].charAt(k) == sporocilo.charAt(i+k) && zamenjava[j][0].charAt(k) != ''){
        k++;
      }
      if(k == zamenjava[j][0].length){
        zam = j;
      }
      else if(zamenjava[j][0].charAt(k) > sporocilo.charAt(i+k)){
        break;
      }
    }
    if(zam > 4){
      drugoS = drugoS + zamenjava[zam][1];
      i = i + zamenjava[zam][0].length-1;
    }
    else if(zam > -1){
      drugoS = drugoS + '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/' + zamenjava[zam][1] + '" alt="' + zamenjava[zam][0] + '">';
      i = i + zamenjava[zam][0].length-1;
    }
    else{
      drugoS = drugoS + sporocilo.charAt(i);
    }
  }
  return drugoS;
};

Klepet.prototype.filtriraj = function (sporocilo){
  var drugoS='';
  for(var i=0;i<sporocilo.length;i++){
    if(sporocilo.charAt(i).toLowerCase() >= 'a' && sporocilo.charAt(i).toLowerCase() <= 'z'){
      var zam=-1;
      for(var j=0;j<word.length;j++){
        var k=0;
        while(word[j].charAt(k)==sporocilo.charAt(i+k).toLowerCase() && word[j].charAt(k) > ''){
          k++;
        }
        if(k==word[j].length && (sporocilo.charAt(i+k).toLowerCase() < 'a' || sporocilo.charAt(i+k).toLowerCase() > 'z')){
          zam=j;
        }
      }
      if(zam>-1){
        for(var n=0;n<word[zam].length;n++){
          drugoS = drugoS + '*';
        }
        i = i + word[zam].length;
        drugoS = drugoS + sporocilo.charAt(i);
      }
      else{
        while(sporocilo.charAt(i)>= 'a' && sporocilo.charAt(i)<= 'z'){
          drugoS = drugoS + sporocilo.charAt(i);
          i++;
        }
        drugoS = drugoS + sporocilo.charAt(i);
      }
    }
    else{
      drugoS = drugoS + sporocilo.charAt(i);
    }
  }
  return drugoS;
}

var word;
readFile('swearWords.txt');

function readFile (path) {
    var oReq = new XMLHttpRequest();
    oReq.open("GET", path, true);
    oReq.onload = function(e) {
      word = oReq.responseText.toLowerCase().split('\n').sort();
    };
    oReq.send();
}

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal,geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    novgeslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ').trim();
      if(kanal.charAt(0)=='"'){
        kanal=kanal.split('"');
        if(kanal[0]=='' && kanal[2].trim()=='' && kanal[4]=='' && kanal.length==5){
          this.spremeniKanal(kanal[1],kanal[3]);
        }
        else{
          this.spremeniKanal(besede.join(' '),'');
        }
      }
      else{
        this.spremeniKanal(besede.join(' '),'');
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var zasebno = besede.join(' ').trim().split('"');
      if(zasebno[0] == '' && zasebno[2].trim() == '' && zasebno[4] == '' && zasebno.length == 5){
        zasebno[3] = this.filtriraj(zasebno[3]);
        zasebno[3] = this.predelaj(zasebno[3]);
        this.socket.emit('zasebno', {
          besedilo: zasebno[3],
          oseba: zasebno[1]
        });
      }
      else{
        sporocilo = 'Neznan ukaz.';
      }
      break;
      default:
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};